pyenv local 2.7.6

cd package

export GRAPHITE_ROOT=/occam/simulator-707e0416-a372-11e4-9c8a-001fd05bb228/package
export BENCHMARKS_ROOT=/occam/benchmark-28fbf608-a38a-11e4-80fe-001fd05bb228/package

make -C tools/hooks
make -C parsec
