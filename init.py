# init.py - set up input in working directory and form command to run benchmark.

# Bad stuff: fluidanimate takes only a power of two number of threads
#            x264 test segfaults on >1 thread
#            ferret test just hangs

import sys
import os
import subprocess
import json

from occam import Occam

# Gather object information

object = Occam.load()

# Open configuration

data = object.configuration("General")

if 'binary' in data:
  benchmark = data["binary"]
else:
  benchmark = "blackscholes"

if 'num_cores' in data:
  cores = data["num_cores"]
else:
  cores = 2

if 'input_set' in data:
  input_set = data["input_set"]
else:
  input_set = "simlarge"

# Gather paths
scripts_path   = os.path.dirname(__file__)
benchmark_path = "%s/package" % (scripts_path)
job_path       = os.getcwd()

apps_path = "%s/parsec/parsec-2.1/pkgs/apps" % (benchmark_path)
bin_path  = "%s/%s/inst/amd64-linux.gcc-sniper/bin" % (apps_path, benchmark)
benchmark_binary = "%s/%s" % (bin_path, benchmark)

# Unpack input
if benchmark != "swaptions":
  os.system("tar xvf %s/%s/inputs/input_%s.tar" % (apps_path, benchmark, input_set))

if benchmark == "blackscholes":
  # Add cores as a command line argument
  benchmark_binary = "%s %s" % (benchmark_binary, cores)

  # Negotiate input
  if input_set == "simdev":
    input_file = "in_16.txt"
  elif input_set == "simsmall":
    input_file = "in_4K.txt"
  elif input_set == "simmedium":
    input_file = "in_16K.txt"
  elif input_set == "simlarge":
    input_file = "in_64K.txt"
  else: # test
    input_file = "in_4.txt"

  # Add input as a command line argument
  benchmark_binary = "%s %s" % (benchmark_binary, input_file)

  # Add output as a command line argument
  benchmark_binary = "%s prices.txt" % (benchmark_binary)
elif benchmark == "bodytrack":
  # Negotiate input
  if input_set == "simdev":
    input_file          = "sequenceB_1"
    input_size          = "1"
    number_of_particles = "100"
    annealing_layers    = "3"
  elif input_set == "simsmall":
    input_file          = "sequenceB_1"
    input_size          = "1"
    number_of_particles = "1000"
    annealing_layers    = "5"
  elif input_set == "simmedium":
    input_file          = "sequenceB_2"
    input_size          = "2"
    number_of_particles = "2000"
    annealing_layers    = "5"
  elif input_set == "simlarge":
    input_file          = "sequenceB_4"
    input_size          = "4"
    number_of_particles = "4000"
    annealing_layers    = "5"
  else: # test
    input_file          = "sequenceB_1"
    input_size          = "1"
    number_of_particles = "5"
    annealing_layers    = "1"

  # Add input as a command line argument
  benchmark_binary = "%s %s 4 %s %s %s 0" % (benchmark_binary, input_file, input_size,
                                             number_of_particles, annealing_layers)

  # Add cores as a command line argument
  benchmark_binary = "%s %s" % (benchmark_binary, cores)
elif benchmark == "facesim":
  # Facesim needs an Storytelling/output directory, otherwise it will create one with
  # a system() call and all hell breaks loose

  # So... we'll just do it here:
  os.system("mkdir -p Storytelling/output")

  # Input is just in FaceData path and is just read from what is untarred.

  # Add timing parameter
  benchmark_binary = "%s -timing" % (benchmark_binary)

  # Add cores as a command line argument
  benchmark_binary = "%s -threads %s" % (benchmark_binary, cores)
elif benchmark == "ferret":
  # Negotiate Input
  if input_set == "simdev":
    top_k = "5"
    depth = "5"
  elif input_set == "simsmall":
    top_k = "10"
    depth = "20"
  elif input_set == "simmedium":
    top_k = "10"
    depth = "20"
  elif input_set == "simlarge":
    top_k = "10"
    depth = "20"
  else: # test
    top_k = "1"
    depth = "1"

  # Add input command arguments
  benchmark_binary = "%s corel lsh queries %s %s" % (benchmark_binary,
                                                     top_k, depth)

  # Add number of threads
  benchmark_binary = "%s %s" % (benchmark_binary, cores)

  # Add output file to command line
  benchmark_binary = "%s output.txt" % (benchmark_binary)
elif benchmark == "fluidanimate":
  # Add number of threads
  benchmark_binary = "%s %s" % (benchmark_binary, cores)

  # Negotiate input
  if input_set == "simdev":
    input_file = "in_15K.fluid"
    input_size = "3"
  elif input_set == "simsmall":
    input_file = "in_35K.fluid"
    input_size = "5"
  elif input_set == "simmedium":
    input_file = "in_100K.fluid"
    input_size = "5"
  elif input_set == "simlarge":
    input_file = "in_300K.fluid"
    input_size = "5"
  else: # test
    input_file = "in_5K.fluid"
    input_size = "1"

  # Add input
  benchmark_binary = "%s %s %s" % (benchmark_binary, input_size, input_file)

  # Add output file
  benchmark_binary = "%s out.fluid" % (benchmark_binary)
elif benchmark == "freqmine":
  # Number of threads is read from OMP_NUM_THREADS
  # Add input
  if input_set == "simdev":
    input_file = "T10I4D100K_1k.dat"
    input_size = "3"
  elif input_set == "simsmall":
    input_file = "kosarak_250k.dat"
    input_size = "220"
  elif input_set == "simmedium":
    input_file = "kosarak_500k.dat"
    input_size = "410"
  elif input_set == "simlarge":
    input_file = "kosarak_990k.dat"
    input_size = "790"
  else: # test
    input_file = "T10I4D100K_3.dat"
    input_size = "1"

  benchmark_binary = "%s %s %s" % (benchmark_binary, input_file, input_size)
elif benchmark == "raytrace":
  # The binary to run is called 'rtview'
  benchmark_binary = "%s/rtview" % (bin_path)

  # Add input
  if input_set == "simdev":
    input_file = "bunny.obj"
    res_width  = "16"
    res_height = "16"
  elif input_set == "simsmall":
    input_file = "happy_buddha.obj"
    res_width  = "480"
    res_height = "270"
  elif input_set == "simmedium":
    input_file = "happy_buddha.obj"
    res_width  = "960"
    res_height = "540"
  elif input_set == "simlarge":
    input_file = "happy_buddha.obj"
    res_width  = "1920"
    res_height = "1080"
  else: # test
    input_file = "octahedron.obj"
    res_width  = "1"
    res_height = "1"

  # Basic command
  command = ' '.join(['-nodisplay', '-automove', '-frames 1'])
  benchmark_binary = "%s %s" % (benchmark_binary, command)

  # Add number of threads
  benchmark_binary = "%s -nthreads %s" % (benchmark_binary, cores)
elif benchmark == "swaptions":
  # Add basic commands and input size
  if input_set == "simdev":
    benchmark_binary = "%s -ns 3 -sm 50" % (benchmark_binary)
  elif input_set == "simsmall":
    benchmark_binary = "%s -ns 16 -sm 5000" % (benchmark_binary)
  elif input_set == "simmedium":
    benchmark_binary = "%s -ns 32 -sm 10000" % (benchmark_binary)
  elif input_set == "simlarge":
    benchmark_binary = "%s -ns 64 -sm 20000" % (benchmark_binary)
  else: # test
    benchmark_binary = "%s -ns 1 -sm 5" % (benchmark_binary)

  # Add cores as a command line argument
  benchmark_binary = "%s -nt %s" % (benchmark_binary, cores)
elif benchmark == "vips":
  # Number of threads is specified in OMP_NUM_THREADS

  # Specify benchmark input
  benchmark_binary = "%s im_benchmark" % (benchmark_binary)

  # Negotiate input
  if input_set == "simdev":
    input_file = "barbados_256x288.v"
  elif input_set == "simsmall":
    input_file = "pomegranate_1600x1200.v"
  elif input_set == "simmedium":
    input_file = "vulture_2336x2336.v"
  elif input_set == "simlarge":
    input_file = "bigben_2662x5500.v"
  else: # test
    input_file = "barbados_256x288.v"

  # Add input as a command line argument
  benchmark_binary = "%s %s" % (benchmark_binary, input_file)

  # Add output file
  benchmark_binary = "%s output.v" % (benchmark_binary)
elif benchmark == "x264":
  # Form basic command
  command = ' '.join(["--quiet", "--qp 20", "--partitions b8x8,i4x4", "--ref 5",
                      "--direct auto", "--b-pyramid", "--weightb", "--mixed-refs",
                      "--no-fast-pskip", "--me umh", "--subme 7",
                      "--analyse b8x8,i4x4"])

  benchmark_binary = "%s %s" % (benchmark_binary, command)

  # Add thread count
  benchmark_binary = "%s --threads %s" % (benchmark_binary, cores)

  # Add input
  if input_set == "simdev":
    input_file = "eledream_64x36_3.y4m"
  elif input_set == "simsmall":
    input_file = "eledream_640x360_8.y4m"
  elif input_set == "simmedium":
    input_file = "eledream_640x360_32.y4m"
  elif input_set == "simlarge":
    input_file = "eledream_640x360_128.y4m"
  else: # test
    input_file = "eledream_32x18_1.y4m"

  benchmark_binary = "%s -o eledream.264 %s" % (benchmark_binary, input_file)

# Tell OCCAM how to run DRAMSim2
Occam.report(benchmark_binary)
