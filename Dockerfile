# DOCKER-VERSION 0.11.1
FROM occam/onuw25lmmf2g64q--knxgs4dfoi------
ADD . /occam/benchmark-PARSEC-2.1
RUN echo 'y' | apt-get install autoconf
RUN echo 'y' | apt-get install xsltproc
RUN echo 'y' | apt-get install pkg-config
RUN echo 'y' | apt-get install gettext
RUN echo 'y' | apt-get install libx11-dev
RUN echo 'y' | apt-get install libxext-dev
RUN echo 'y' | apt-get install libxt-dev
RUN echo 'y' | apt-get install libxmu-dev
RUN echo 'y' | apt-get install libxi-dev
RUN echo 'y' | apt-get install imagemagick
RUN echo 'y' | apt-get install libjpeg-dev
RUN cd /occam/benchmark-PARSEC-2.1; /bin/bash -c 'source /occam/setup_python.sh; source /occam/setup_perl.sh; cd /occam/benchmark-PARSEC-2.1; bash /occam/benchmark-PARSEC-2.1/build.sh'
VOLUME ["/occam/benchmark-PARSEC-2.1"]
CMD cd /occam/benchmark-PARSEC-2.1; /bin/bash -c 'source /occam/setup_python.sh; source /occam/setup_perl.sh; cd /job;  pyenv local 2.7.6; python /occam/benchmark-PARSEC-2.1/launch.py'
